function [QNew] = DistObj(Q, stats, centers, radii, px, BBTrans)
%%
    if nargin == 5
        BBTrans = false;
    end
%%
    % Get the bounding boxes of the respective objects
    bb = zeros(length(stats),4);
    for object = 1:length(stats)
        bb(object,:) = round(stats(object).BoundingBox); 
    end

    D_hyp = zeros(1,length(stats)); %Prealloc
    %For each object, calculate the shortest distance between the target
    %and the perimeter of the plate
    for i = 1:length(stats)
        if Q(4,i) %Left half plane
            D_hyp(1,i) = abs(sqrt((bb(i,1)-centers(1))^2 + (bb(i,2)-centers(2))^2) - radii);
        else %Right half plane
            D_hyp(1,i) = abs(sqrt((bb(i,1)+bb(i,3)-centers(1))^2 + (bb(i,2)+bb(i,4)-centers(2))^2) - radii);
        end
    end

    %Preallocate matrices and arrays
    Ymin = zeros(1,numel(stats));
    Xmin = zeros(1,numel(stats));
    Angmin = zeros(1,numel(stats));
    y_dist = zeros(numel(stats),numel(stats));
    x_dist = zeros(numel(stats),numel(stats));
    ang_dist = zeros(numel(stats),numel(stats));
    L = zeros(1,numel(stats));
    S = zeros(1,numel(stats));
    R = zeros(1,numel(stats));
    Dmin = zeros(1,numel(stats));
    Angthresh = 13;

    %Calculate the horizontal- and vertical distance between each of the
    %objects and store them the respective matrix, x_dist and y_dist.
    %Also calculate the angle between the objects.
    for k = 1:numel(stats)
        for i = 1:numel(stats)
            if k == i %Don't give a value for the same object
                 y_dist(k,i) = NaN;
                 x_dist(k,i) = NaN;
                 ang_dist(k,i) = NaN;
            else
                y_dist(k,i) = abs(stats(k).Centroid(2)-stats(i).Centroid(2)); %Distance in Y
                x_dist(k,i) = abs(stats(k).Centroid(1)-stats(i).Centroid(1)); %Distance in X
                ang_dist(k,i) = atand(y_dist(k,i)/x_dist(k,i)); %Angle in degrees
            end       
        end
        [Ymin(k),L(k)] = min(y_dist(k,:));
        [Xmin(k),S(k)] = min(x_dist(k,:));
        [Angmin(k),R(k)] = min(ang_dist(k,:));
    end

   % Sort y_dist by ascending order and pick out the three lowest values
   % and store them in Ymin, which is nx6 where the three first columns
   % represent the y-pxLength and the last three columns represent the
   % nearest object in Y-length
   Yval = zeros(length(y_dist),length(y_dist)); %Prealloc
   Ycol = zeros(length(y_dist),length(y_dist));
   Ymin = zeros(length(Ycol),numel(stats));
    for i = 1:length(y_dist)
        [Yval(i,:), Ycol(i,:)] = sort(y_dist(i,:)); %Sort in asc.order and store represening value and column in two arrays
    end
    if numel(stats) >= 3
        for i = 1:length(y_dist) %Concatenate the arrays      
            Ymin(i,1:3) = y_dist(i,Ycol(i,1:3)); %Ymin is defined as [dist2obj..., ClosestObj...]
            Ymin(i,4:6) = Ycol(i,1:3);
        end
    else
        for i = 1:length(y_dist)
        Ymin(i,1) = y_dist(i,Ycol(i,1));
        Ymin(i,2) = Ycol(i,1);
        end
    end
           
    % Sort x_dist by ascending order in a similar nx6 matrix as for Ymin
    Xval = zeros(length(x_dist),length(x_dist));
    for i = 1:length(x_dist)
        [Xval(i,:),~] = sort(x_dist(i,:));
    end
    Xmin = zeros(length(Ycol),numel(stats)); %Concatenate the arrays
    if numel(stats) >= 3
        for i = 1:length(x_dist)        
            Xmin(i,1:3) = x_dist(i,Ycol(i,1:3)); %Xmin is defined as [dist2obj..., ClosestObj...
            Xmin(i,4:6) = Ycol(i,1:3);
            numObj = 3;
            numCord = 3;
        end
    else
        for i = 1:length(x_dist)
            Xmin(i,1) = x_dist(i,Ycol(i,1));
            Xmin(i,2) = Ycol(i,1);
            numObj = 2;
            numCord = 0;
        end       
    end
    
    % Of these three objects, calculate which one of them has the shortest X-distance:
    XD = zeros(1,length(Xmin)); %Array for storing the horizontal distance between objects
    ObjXmin = zeros(1,length(Xmin)); %Array for storing the corresponding object on the plate with lowest relative distance
    SizeXmin = size(Xmin);
    for i = 1:SizeXmin(1)
        [~,b] = min(Xmin(i,1:numObj));
        ObjXmin(i) = Xmin(i,numCord+b);
        if ang_dist(i,ObjXmin(i)) < Angthresh
            XD(i) = x_dist(i,ObjXmin(i)); %Take the lowest value
        else
            XD(i) = x_dist(i,R(i)); %Take one with the lowest angle between them
        end
    end
        
    % Create a nx4 matrix that has the corresponding values for each
    % row: spoon travel distance, grasping polarity change (1=change),
    % highest proximity object and object index.
    for i = 1:length(Angmin)
        if Angmin(i) < Angthresh && y_dist(i,L(i)) < bb(L(i),4)/2 && XD(i) < D_hyp(i)
            Dmin(1,i) = round(XD(i)/px,1); %Input target distance
            %Shift direction polarity if object the nearby object is
            %further away in positive X-direction
            if (Q(4,i) && stats(i).Centroid(1) < stats(ObjXmin(i)).Centroid(1))
                Dmin(2,i) = 1;
            elseif ~Q(4,i) && stats(i).Centroid(1) > stats(ObjXmin(i)).Centroid(1)
                Dmin(2,i) = 1;
            else
                Dmin(2,i) = 0;
            end
            Dmin(3,i) = ObjXmin(i);
            Dmin(4,i) = i;
        else
            Dmin(1,i) = round(D_hyp(i)/px,1); %If a nearby object is not suitable, input the shortest distance to the plate perimeter instead
            %Dmin(2,i) = 0;
            Dmin(3,i) = 0;
            Dmin(4,i) = i;
        end
    end

    % Set the outpt matrix to correct calcluated polarities and add the row
    % for spoon travel distance
    Q( :, ~any(Q,1) ) = []; %Remove any total-zero columns from input matrix
    QNew = zeros(5,numel(stats));
    for i = 1:numel(stats)
        if Dmin(2,i)
            QNew(4,i) = ~Q(4,i);
        else
            QNew(4,i) = Q(4,i);
        end
    end
    QNew(1:3,:) = Q(1:3,:);
    QNew(5,:) = Dmin(1,:);    
    
    %% Convert coordinates with respect to their bounding boxes
    
    if BBTrans %Do only if specified with BBTrans = true as function input
        XNew = zeros(1,length(stats)); %Prealloc array for storing new X-coordinates
        XYMax = zeros(1,length(stats)); %Prealloc array for storing maximum XY-values inside circle radius
        C2bbDist = zeros(1,length(stats)); %Prealloc array for spoon travel distance correction
        for k = 1:length(stats)
            XYMax(k) = sqrt(QNew(1,k)^2 + QNew(2,k)^2); %Calculate maximum XY-coordinates
            if QNew(4,k) == 0 %Grasp from the left
               XNew(1,k) = QNew(1,k) - bb(k,3)/(2*px); %X coordinate, grasp from middle of left bb
               C2bbDist(1,k) = abs(abs(XNew(1,k))-abs(QNew(1,k))); %Distance from center coord to bb edge
            else
               XNew(1,k) = QNew(1,k) + bb(k,3)/(2*px); %X coordinate, grasp from middle of right bb
               C2bbDist(1,k) = abs(abs(XNew(1,k))+abs(QNew(1,k))); %Distance from center coord to bb edge
            end
            
            % Limit coordinates to inside plate radius
            if XNew(1,k) > XYMax(k)
                XNew(1,k) = XYMax;
            elseif XNew(1,k) < -XYMax(k)
                XNew(1,k) = -XYMax(k);
            end
        end
        
        %Insert calculated coordinates into QNew Matrix
        QNew(1,:) = round(XNew,1); %Insert new calculated X-coordinates into output Q matrix, rounded to one decimal point
        QNew(5,:) = QNew(5,:)-C2bbDist; %Correct for spoon travel distance
        for k = 1:length(stats)
            if QNew(5,k) < 0 %Make sure travel distance is not negative
                QNew(5,k) = 0.1; %Set minimum spoon travel distance to 0.1cm
            elseif QNew(5,k) > 5
                QNew(5,k) = 5; %Set maximum spoon travel distance to 5cm
            end
        end
    end
            
   
end
