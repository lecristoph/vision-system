%Input is as follows: 
% [stats = trackObject(img, Color, LowerLimit, UpperLimit)
% Color arguments: Red, Green, Blue, All, Blob
% If Color = 'All', then the whole RGB spectrum will be included
%
% Author:
% Christoffer Karlsson, for Camanio Care.
% Electrical Engineering @
% Karlstad University, 2018.
% email: christoffer.karlsson@europe.com
%


function[stats] = trackObject(img, Color, LL, UL)
%%
EccThresh = 0.978;

img = AutoEnhance(img, 'HSV', 1.2); %Increase saturation by factor 1.2
%Preallocating difference image variable
img_Size = size(img);
xMax_C = img_Size(2);
yMax_C = img_Size(1);
diff_im = struct('Color', zeros(xMax_C,yMax_C)); 


    % To track the colored objects
    % we have to subtract the colored component 
    % from the grayscale image and then extract the colored components in the image.
    if strcmp(Color,'All') || strcmp(Color,'all')
        for n = 1:3
         diff_im(n).Color = imsubtract(img(:,:,n), rgb2gray(img));
         
         %Use a median filter to filter out noise
         diff_im(n).Color = medfilt2(diff_im(n).Color, [5 5]);
         % Convert the resulting grayscale image into a binary image.
         diff_im(n).Color = im2bw(diff_im(n).Color,0.15);
         
         % Remove all those pixels outside limit of LL and UL
         diff_im(n).Color = xor(bwareaopen(diff_im(n).Color,LL),  bwareaopen(diff_im(n).Color,UL));
        end               
         % Label all the connected components in the image.
         diff_im = diff_im(1).Color + diff_im(2).Color + diff_im(3).Color;
         diff_im = logical(diff_im);
        
         % Here we do the image blob analysis.
         % We get a set of properties for each labeled region.
         stats = regionprops(diff_im, 'BoundingBox', 'Centroid');
                
    elseif strcmp(Color,'Red') || strcmp(Color,'Green') || strcmp(Color,'Blue')
        switch Color
            case 'Red'
                Color = 1;
            case 'Green'
                Color = 2;
            case 'Blue'
                Color = 3;
        end
        diff_im = imsubtract(img(:,:,Color), rgb2gray(img));
               
        %Use a median filter to filter out noise
        diff_im = medfilt2(diff_im, [5 5]);
        % Convert the resulting grayscale image into a binary image.
        diff_im = im2bw(diff_im,0.15);
    
        % Remove all those pixels outside limit of LL and UL
        diff_im = xor(bwareaopen(diff_im,LL),  bwareaopen(diff_im,UL));
       
        % Convert to logical image
        diff_im = logical(diff_im);
    
        % Here we do the image blob analysis.
        % We get a set of properties for each labeled region.
        stats = regionprops(diff_im, 'BoundingBox', 'Centroid');

    elseif strcmp(Color,'Blob') || strcmp(Color,'blob')
        
        hsvImage = rgb2hsv(img); %Converts to HSV
        
        %Create yellow channel
        yellowIndex=repmat((hsvImage(:,:,1)>1/360)&(hsvImage(:,:,1)<80/360),[1 1 3]);   
        yellow=hsvImage.*yellowIndex;

        %Change hue of yellow channel
        hueFactor=0.1;
        yellowHue=yellow(:,:,1)*hueFactor;
        yellow(:,:,1)=yellowHue;

        %Create green channel
        greenIndex=repmat((hsvImage(:,:,1)>105/360)&(hsvImage(:,:,1)<140/360),[1 1 3]);   
        green=hsvImage.*greenIndex;

        %Change hue of green channel
        greenHue=green(:,:,1)*hueFactor;
        green(:,:,1)=greenHue;        
        
        %Create aquamarine channel
        aquaIndex=repmat((hsvImage(:,:,1)>75/360)&(hsvImage(:,:,1)<110/360),[1 1 3]);   
        aqua=hsvImage.*aquaIndex;

        %Increase gain of aquamarine channel
        aquaHue=aqua(:,:,2)*5;
        aqua(:,:,2)=aquaHue;
        
        %Set values for each created channel
        hsvImage(yellowIndex)=yellow(yellowIndex);
        hsvImage(greenIndex)=green(greenIndex);
        hsvImage(aquaIndex)=aqua(aquaIndex);
        
        %Split the HSV channels:
        hChannel = hsvImage(:, :, 1); %Hue
        sChannel = hsvImage(:, :, 2); %Saturation
        vChannel = hsvImage(:, :, 3); %Value

        newV = 1.2 * (vChannel); % Increase contrast by factor 
        newS = 2.4 * (sChannel); % Increase gain saturation by factor 
        newH = 1.0 * hChannel; % Increase hue by factor 

        newHSVImage = cat(3, newH, newS, newV); %concatenates the channels into one image (HSV)
        img = hsv2rgb(newHSVImage); %Converts back to rgb
        
         % Convert to grayscale
         img = rgb2gray(img);
         
         %Filter out regions with low contrast
         img = imadjust(img,[0 0.7],[0.1 1]);
         
         % Use a median filter to filter out noise
         img = medfilt2(img, [5 5]);
         % Convert the resulting grayscale image into a binary image.
         bw = imbinarize(img,'adaptive','ForegroundPolarity','dark','Sensitivity',0.0097);
         % Invert the binary image
         bw = imcomplement(bw);
         % Remove all those pixels outside limit of LL and UL
         bw = xor(bwareaopen(bw,floor(LL)), bwareaopen(bw,ceil(UL)));
         
         % Convert to logical image
         bw = logical(bw);
    
         % Here we do the image blob analysis.
         % We get a set of properties for each labeled region.
         stats = regionprops(bw, 'BoundingBox', 'Centroid', 'Eccentricity','Area');
         
         %Preallocate
         J = zeros(1,numel(stats));
         Newstats = stats;
         
         %Remove straight lines
         for k = 1:numel(stats)
             J(k) = stats(k).Eccentricity;
             if J(k) > EccThresh
                 Newstats(k) = [];
             end
         end
         stats = Newstats;
           
    else
        msgbox('Invalid Value of variable "Color".', 'warning','error');
    end
end




