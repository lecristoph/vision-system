%This function converts a RGB image to HSV and increases contrast by a
%factor (1.5)
%
% Author:
% Christoffer Karlsson, for Camanio Care.
% Electrical Engineering @
% Karlstad University, 2018.
% email: christoffer.karlsson@europe.com
%
function[img] =  AutoEnhance(img, Method, Z, T)
%% Metod 1: RGB 
switch Method
    case 'RGB'
        if nargin == 2
            Z = 15;
        end

        h = ones(5,5)/Z; %Lower denominator gives larger highlights. Z=15 to 20 is usually good.

        img = imfilter(img,h);

%% Metod 2: HSV
    case 'HSV'
        if nargin == 2
            Z = 1.1;
        end

    
hsvImage = rgb2hsv(img); %Converts to HSV
%Split the HSV channels:
hChannel = hsvImage(:, :, 1); %Hue
sChannel = hsvImage(:, :, 2); %Saturation
vChannel = hsvImage(:, :, 3); %Value

        if nargin == 4
            if strcmp(T,'Contrast')
            newV = Z * (vChannel); % Increase contrast by factor
            newS = 1 * (sChannel); % Increase gain saturation by factor
            newH = 1 * hChannel; % Increase hue by factor
            end
            if strcmp(T,'Saturation')
            newS = Z * (sChannel); % Increase gain saturation by factor
            newH = 1 * hChannel; % Increase hue by factor
            newV = 1 * (vChannel); % Increase contrast by factor
            end
            if strcmp(T,'Hue')
            newH = Z * hChannel; % Increase hue by factor
            newS = 1 * (sChannel); % Increase gain saturation by factor
            newV = 1 * (vChannel); % Increase contrast by factor
            end
        elseif nargin == 3 || nargin == 2
            newS = Z * (sChannel); % Increase gain saturation by factor as default
            newH = 1 * hChannel; % Increase hue by factor
            newV = 1 * (vChannel); % Increase contrast by factor
            
        end

        newHSVImage = cat(3, newH, newS, newV); %concatenates the channels into one image (HSV)
        img = hsv2rgb(newHSVImage); %Converts back to rgb
        %img = imadjust(img,[0.05 0.65],[]); %Increase contrast and cut out certain frequencies of light

    %%
    case 'LAB'
        LAB = rgb2lab(img);
        l = LAB(:,:,1);
        a = LAB(:,:,2);
        b = LAB(:,:,3);

        l = l*1.2;
        a = a*1;
        b = b*1;

        NewLAB = cat(3,l,a,b);
        img = lab2rgb(NewLAB);
end

end