% Software by:
% Christoffer Karlsson, for Camanio Care.
% Electrical Engineering @
% Karlstad University, 2018.
% email: christoffer.karlsson@europe.com
%
% This software will estimate the coordinate of a given object on a plate
% within 1cm. If you encounter any errors when running, try adjusting the
% camera so that the front base of Bestic is clearly visible, as well as
% the plate. The software has been evaluated using camera angles ranging
% from 0 to 35 degrees and camera height from 35 cm up to 65 cm.
% The camera must be suited to the left of the Bestic and the plate and the
% preferred cameras to use are Intel RealSense R200 and Intel RealSense
% D400 series, but any camera with high enough resolution and narrow enough
% focal length will work in theory. Note that you may need to calibrate the
% camera is using a camera other than the ones mentioned above.
%
% If it is the first time you run this program, please set the variable
% px_flag = false, so that the pixel to centimeters may be calculated.
%
% Before running the program, input the corresponding values:
% px ( 1cm in pixels )
% PlateDiam (Diameter of plate in mm)
% psi (camera tilt angle)
%
% Q is the matrix variable where all the position data:
% [x1 x2 x3...; y1 y2 y3...; z1 z2 z3...; graspingDirection...; graspingDistance...] is stored and may be transmitted
% to Bestic.
%
% --- Main functions: ----
% BESTIC DETECTOR: [X, Y, BB, theta, f] = BesticDetector(img)
% Returns the X,Y-position relative image coordinate system, the bouding
% box data around the display area on bestic, the relative angle and the
% line functions. 
% This function incorporates Aggregated channel feature detection for
% Bestic, blob analysis and Hough-transform.
%
% PLATE DETECTOR: [centers, radii, metric] = ACFPlateDetector(img, circle_range, thresh)
% Returns the center coordinate relative camera coordinate system, the
% plate radius and the relative strength of the detected circle relative
% circle center point. This function incorporates ACF, Circular Hough
% Transform and CircleFit.
%
% OBJECT DETECTION ON PLATE: [stats] = trackObject(img, Color, LL, UL)
% Returns the center coordinate and bounding box around the object as a
% struct. If Color == Red, Green, Blue, All: This is a quite simple algorithm that stores the color channel of
% an image into a new variable that is then converted to a binary image and
% blob analysis is performed.
% If Color == 'Blob', Then we perform blob analysis of all objects within
% the inner perimeter of the plate
% -------------------
%
%% Setup camera and variables. This section runs only once when the program is started.

CamIsDef = evalin( 'base', 'exist(''cam'',''var'') == 1' );
tic;
A = true; % Head loop variable. When A == 0, the program will end.
State = 0; %Indicates the current state of the program
RunCount = 0; % Specifies how many times the program has run
xMin_C = 0; %Define start point of image (X,Y)
yMin_C = 0; 
spacing = 1; %Spacing between each pixel (scaling)
px = 38.9; %One centimeter is approximatly 23 pixels in length if height_cam = 60.5cm from plate top
PlateDiam = 205; % Inner diameter across the plate used (in millimeters)
savepath = 'snaps';  %where the files will be saved
ReferenceNameTemplate = 'snapshot_%04d.png';  %name pattern
psi = 35; %Camera pitch angle (Offset angle x-axis)
Color = 'blob' ; %Sets tracking color. (Red = 1, Green = 2, Blue = 3. 'All' includes the wholpe RGB spectrum and 'Blob' includes all "blobs" on the plate)
Z = 20; % Sets threshhold for "AutoEnhancement function".
Frames = 1; %Number of iterations (total amount of snapshots to be processed)
ang_thresh = 5*pi/180; % angle threshhold for needing recalibration (5 degrees)
trans_thresh = px ; % Translational threshhold for needing recalibration (one cm)
thresh_plate = -6; %Sets threshold for plate detection in acf-detector function
thresh_bestic = -10; %Sets threshold for Bestic detection in acf-detector function
thresh_spoon = 1; %Sets threshold for spoon detection in acf-detector function
circle_thresh = 28; %circle detection error margin
circle_range = round([(PlateDiam/10)*px/2 - circle_thresh, (PlateDiam/10)*px/2 + circle_thresh]); % radius range for circle detection (plate)
MaskMethod = 'Fill'; %Methods for Bestic detection algo
SegmentMethod = 'Shape';
px_flag = true; %Run only pixel to centimeter conversion one time. Set to false if px2cm conversion is desired.
BBTrans = true;

if ~CamIsDef %Run only if camera is not already defined
    fprintf('Initializing camera...');
    %cam = webcam(1);
    cam = webcam('Intel(R) RealSense(TM) 415 RGB'); %Initialize camera
    cam.Resolution = char(cam.AvailableResolutions(3)); %Set resolution to 1920x1080
    cam.Sharpness = 65; %Increase sharpness parameter

    if strcmp(cam.Name,'Intel(R) RealSense(TM) 415 RGB')
        CP = load('cameraParamsD415.mat'); %Loads calibration data for D415 camera
    elseif strcmp(cam.Name,'Intel(R) RealSense(TM) 3D Camera (R200) RGB')
        CP = load('cameraParamsR200.mat'); %Loads calibration data for R200 camera
    end

    warning('off', 'Images:initSize:adjustingMag'); %Disable warning for image too big
    fprintf('Done. (%1.1f s) \n', toc);
    pause(2); %Camera need a second to adjust its parameters before it's functional
    CamIsDef = evalin( 'base', 'exist(''cam'',''var'') == 1' );
end

%{
% Set up UART connection with BESTIC
%
B = Bluetooth('HC-06',1); %Initialize Bluetooth connection with Bestic
if exist('B','var') == 1 % Opens up serial connection between PC and BT
fclose(B);
end
fopen(B);

%Makes sure serial connection has been established
if B.Status == 'open'
    disp('Connection established');
    %fwrite(B, 'A'); %Sends command to Bestic 
else disp('Error...');
end
%}

while State < 3 && A
% Define governing state (Which section to be run)
    State = 1;
    
    %% Initialization, calculation of fixed location and initial targets
    %
    % Detection of Bestic, plate and initial target(s). This sections runs for
    % one time during startup where the camera is first initialized and the
    % location of the plate and Bestic is settled. This section may be run more
    % than one time if needed.
    while State == 1
        
        fprintf('Taking snapshot...');
        img = snapshot(cam); %Take snapshot
        
        %Turns the image right way around (The Intel D415 produces a mirrored
        %version of the original image)
        if strcmp(cam.Name,'Intel(R) RealSense(TM) 415 RGB')
            img = flip(img,2); %Horizontal flip
            img = flip(img,1); %Vertical flip
        end
        
        %Correct for white balance errors
        img = AutoWhitebalance(img);
        
        %Set image size constant
        img = imresize(img,[1080 1920]);
        
        % Define camera coordinate system
        img_Size = size(img);
        xMax_C = img_Size(2);
        yMax_C = img_Size(1);
        x_C = xMin_C:spacing:xMax_C;
        y_C = yMin_C:spacing:yMax_C;
        img = undistortImage(img, CP.cameraParams); % Corrects for lens distortion
        img = NoiseRM(img); %Removes color noise from image
        
        fprintf('Done. (%1.1f s) \n', toc);

        fprintf('Locating Bestic...');
        try
            %Locate Bestic location and angle relative camera local X-axis
            [Bx_C,By_C, bb_B, theta, Lines] = BesticDetector(img, thresh_bestic, MaskMethod, SegmentMethod); 
        catch
            fprintf('Bestic could not be detected. Try adjusting the camera or tune the detection parameters. \n');
            fprintf('Exiting program...\n');
            A = false;
            break;
        end
        fprintf('Done. (%1.1f s) \n', toc);
                       
        %Crop out the segment containging Bestic (turn those pixels white)   
        img_cropped = img;
        img_cropped(1:yMax_C,round(bb_B(1)):xMax_C,1:3) = 255;
        
        %Calculate pixels to centimeters
        if ~px_flag
            measure_message = sprintf('Please place the calibration tab in front of the camera.\n');
            uiwait(msgbox(measure_message)); %Display a message box to the user              
            [px,~] = px2cm(img_cropped); %Get pixel value that equals one centimeter
            
            % Define radius range for circle detection (plate perimeter)
            circle_range = round([(PlateDiam/10)*px/2 - circle_thresh, (PlateDiam/10)*px/2 + circle_thresh]);
            Choice = questdlg('Pixels to centimeters conversion successful. Would you like to restart the program?', 'Yes', 'No');
            fprintf('1cm = %1.1f px. New value has been assigned.\n',px);
            px_flag = true;
            switch Choice
                case 'Yes'                   
                    fprintf('Restarting program...\n');
                    tic;
                    break;               
                case 'No'                
                    A = false;
                    State = NaN;
                    break;
            end                                    
        end
        
        fprintf('Locating Plate...');    
        %Calculates the center coordinates and radius of plate
        [centers, radii, metric] = ACFPlateDetector(img_cropped, circle_range, thresh_plate, true); 
        fprintf('Done. (%1.1f s) \n', toc);

        try
            [M,L] = max(metric); % Stores the row of "centers" that has the highest relative strength for circle center into var L
            centers = centers(L,1:2); %Disregard the circle with lower relative strength.
            radii = radii(L); % Same as above
        catch
            [centers, radii, metric] = ACFPlateDetector(img_cropped, circle_range, thresh_plate, false);
            [M,L] = max(metric); % Stores the row of "centers" that has the highest relative strength for circle center into var L
            centers = centers(L,1:2); %Disregard the circle with lower relative strength.
            radii = radii(L); % Same as above
        end

        % Check if spoon is visible
        [img_MaskedSpoon, img_Spoon] = SpoonDetector(img_cropped, centers, radii, thresh_spoon);
        
        % Setting lower limit (pixel area) for object tracking
        % to a 500:th of the area of the plate
        LL = floor((pi*radii^2)/500); 
        % Setting upper limit (pixel area) for object tracking
        % to a third of the area of the plate
        UL = ceil((pi*radii^2)/3); 
        
        % Rotation matrix (around Z-axis)for converting
        % pixel coordinates in image to Bestic coordinates
        Rz = [cos(theta), -sin(theta), 0; sin(theta), cos(theta), 0; ...   
               0, 0, 1];                                                   
        
        State = 2; %Go to the main loop
    end

    %% Main loop
    % This presuppose that the location and orientation of Bestic, as well
    % as the location of the plate has been determined. This loop will
    % continue for as long as the user is eating and may be interrupted every Y
    % number of times in between each cycle to reassure that the 
    % location and orientation of the plate and Bestic has not changed. If
    % Bestic or the plate has moved in any way, recalibration will occur.

    while State == 2 && A

        tic; %Clear and start a new timer
        clc;
        RunCount = RunCount + 1;
                
        for i = 1:Frames
            fprintf('Running iteration number: %d.\n',i);
            fprintf('Taking snapshot...');
            img2 = snapshot(cam); %Take snapshot

            %Turns the image right way around (The Intel D415 produces a mirrored
            %version of the original image)
            if strcmp(cam.Name,'Intel(R) RealSense(TM) 415 RGB')
                img2 = flip(img2,2); %Horizontal flip
                img2 = flip(img2,1); %Vertical flip
            end

            %Correct for white balance errors
            img2 = AutoWhitebalance(img2);
            img2 = undistortImage(img2, CP.cameraParams); % Corrects for lens distortion
            img2 = NoiseRM(img2); %Removes color noise from image

            %Set image to constant size
            img2 = imresize(img2,[1080 1920]);

            %Crop out the segment containging Bestic (turn pixels white)   
            img_cropped = img2;
            img_cropped(1:yMax_C,round(bb_B(1)):xMax_C,1:3) = 255;

            fprintf('Done. (%1.1f s) \n', toc);   

             % Check if spoon is visible
            [img_MaskedSpoon, img_Spoon] = SpoonDetector(img_cropped, centers, radii, thresh_spoon);

            fprintf('Locating objects on plate...');
            img_M = Mask_plate(img_MaskedSpoon, centers, radii); %Removes everything outside of plate
            img_M = AutoEnhance(img_M, 'RGB', Z); %Increase brightness
            img_M = AutoEnhance(img_M,'HSV',1.2,'Contrast'); %Increase contrast
            img_M = imadjust(img_M, [0 1],[0.05 0.9]); %Remove harsh highlights
            stats = trackObject(img_M, Color, LL, UL); %Gets the coordinates of the colored objects
            fprintf('Done. (%1.1f s) \n', toc);

            fprintf('Calculating object coordinates...');

            %Preallocating and resetting object coordinates
            Tx_C = zeros(1, length(stats));
            Ty_C = zeros(1, length(stats));
            P = zeros(3,3);
            
            % -----------------------------------------------
            % Kinematics section
            % -----------------------------------------------
            % Calculate translational transform of coordinates
            for object = 1:length(stats)
                bc = stats(object).Centroid;
                Tx_C(object) = bc(1); % X-coordinate
                Ty_C(object) = bc(2); % Y-coordinate
                P(object,1) = round(Tx_C(object) - centers(1))/px; % Load XYZ vector into matrix, compensate for translation between the
                P(object,2) = round(Ty_C(object) - centers(2))/px; % two coordinate systems (plate center and camera) and conv px -> cm
            end
            P = transpose(P);
            %Convert pixel coordinates of target(s) kinematically for Bestic

            %Preallocating and resetting Q in between iterations   
            Q = zeros(length(Rz), length(P));

            %Calculate new target coordinate matrix with respect to rotation, rounded to one dec.place.
            %The detected object coordinates are stored in their respective column as; 
            %[x1 x2 x3;y1 y2 y3;z1 z2 z3]
            for n = 1:length(P)
               Q(:,n) = round(Rz*P(:,n),1); 
            end

            % Correcting for y-angle rotation (pitch of camera)        
            gamma = (psi*1.05)*pi/180;                   
            u = [0 1 0]; % Y axis, don't make any changes since we have the camera att roll angle approx = 0
            c = cos(gamma); s = sin(gamma); C = 1-c; %Construct row for pitch
            xC = u(1)*C; yC = u(2)*C; zC = u(3)*C; %Compute elements in homography matrix
            xs = u(1)*s; ys = u(2)*s; zs = u(3)*s;
            xyC = u(1)*yC; zxC = u(3)*xC; yzC = u(2)*zC;
            tm = [u(1)*xC+c  xyC-zs  zxC+ys; ... % Construct rotational matrix about y-axis from computed elements
            xyC+zs  u(2)*yC+c  yzC-xs; ...       % using formula from https://en.wikipedia.org/wiki/Homography
            zxC-ys  yzC+xs  u(3)*zC+c];
            %Since we are considering the Z-coordinate to be equal to zero,
            %set the third row of tm = [0 0 1]
            tm(3,:) = [0 0 1];
            %The end result will be 
            %tm =[cos(gamma) 0 sin(gamma); 0 1 0; 0 0 1] (only pitch
            %included), but in order to have some scaling if we want to include roll
            % or yaw factor in the future, keep this code snippet here.
                       
            % Extract objects with X > 0
            Q2 = zeros(3,length(Q));
            for k = 1:length(Q)
                if Q(1,k) > -0.5 && Q(1,k) < 5
                    Q2(:,k) = Q(:,k);
                end
            end

            % Perform transformation
            Q2 = round(tm*Q2,1);

            % Put them back
            for k = 1:length(Q2)
                if Q2(1,k) == 0 && Q2(2,k) == 0 && Q2(3,k) == 0
                    Q2(:,k) = Q(:,k);
                end
            end
                        
            % -----------------------------------------------
            % Coordinate filtering
            % -----------------------------------------------
            %Remove objects if they are outside the perimeter of the plate
            Rmax = radii/px; %Maximum x,y-value
            DA = zeros(1,length(Q2)); %Preallocate array
            for n = 1:length(Q2)
                if abs(Q2(1,n)) > Rmax || abs(Q2(2,n)) > Rmax
                    DA(n) = 0; %Remove
                else
                    DA(n) = 1; %Keep
                end
            end
            % Create a logical array where 0 indicates to remove the object
            DA = logical(DA);
            % Remove based on logical index
            Q2 = Q2(:,DA);
            
            % -----------------------------------------------
            % Calculate grasping direction and spoon travel distance whilst
            % grasping target
            % -----------------------------------------------
            % Calculate what direction to grasp object from.
            % Direction is stored in the 4:th row for each object (column
            % in matrix), where 1 represents grasp target from the right
            % and a zero represents grasping from the left.
            Q = AttackDir(Q2); % Set initial values by looking at target's proximity to the plate perimeter
            img_Size = size(img2);
            
            % Set direction based on nearby objects and calculate an approximate spoon travel distance
            if numel(stats) >= 2
                Q = DistObj(Q, stats, centers, radii, px, BBTrans); 
            else
                %This section runs if there is only one object on the plate
                Q( :, ~any(Q,1) ) = []; %Remove any total-zero columns from input matrix
                bb = zeros(length(stats),4); %Prealloc
                for object = 1:length(stats) % Get the boundingbox
                    bb(object,:) = round(stats(object).BoundingBox); 
                end
                % Calculate distance to plate perimeter
                if Q(4,1) %Object is on the left half plane
                    D_hyp(1,1) = abs(sqrt((bb(1,1)-centers(1))^2 + (bb(1,2)-centers(2))^2) - radii);
                else %Object is on the right half plane
                    D_hyp(1,1) = abs(sqrt((bb(1,1)+bb(1,3)-centers(1))^2 + (bb(1,2)+bb(1,4)-centers(2))^2) - radii);
                end
                Q(5,1) = round(D_hyp/px,1); %Store the travel distance data in coordinate matrix,row 5.              
            end
            
            %-------------------------------------------------------%
            % --- Get object closest to center, store in var QB --- %
            % ---- This is the coord. that is sent to Bestic ------ % 
            %-------------------------------------------------------%
            Qd = zeros(1,length(stats)); %Preallocate array
            %Calculate absolute value of x,y-positions
            for k = 1:length(stats)
                Qd(k) = sqrt(Q(1,k)^2+Q(2,k)^2);
            end
            [~,col] = min(Qd); %Get object closest to center
            QB = Q(:,col);
            
            fprintf('Done. (%1.1f s)\n',toc);
            fprintf('Iteration %d complete\n',i);

            % -----------------------------------------------
            % Display and print image to file
            % -----------------------------------------------
            %Display coordinates
            QSize = size(Q);
            for n = 1:QSize(2)
                fprintf('Target %d: X: %1.1f  Y:%1.1f  Z:%1.1f\n',n,Q(1,n),Q(2,n),Q(3,n));
            end

           %Write image to file
           thisfile = sprintf(ReferenceNameTemplate, Frames);  %create filename
           fullname = fullfile(savepath, thisfile);  %folder and all
           imwrite(img2, fullname);  %saves the image as png
           imwrite(img_M,fullfile(savepath, sprintf('snapshot_%04d_masked.png', Frames)));

        end  
        
        fprintf('Loop finished. Total snapshots taken: %d. Total elapsed time: %1.1f \n',Frames, toc);
        fprintf('Mean processing time for each iteration: %1.1f s\n', toc/Frames);
        
        % -----------------------------------------------
        % Check if Bestic is still aligned with the plate
        % -----------------------------------------------       
        %Initial measurments
        dx = Bx_C - centers(1); % Horizontal distance between plate center and bestic
        dy = By_C - centers(2); % Vertical distance between plate center and bestic
        alpha = atan(dy/dx); %Angle between plate center and Bestic display center point [degrees]

        fprintf('Locating Bestic...');
        try
            %Locate Bestic location and angle relative camera local X-axis
            [Bx2_C,By2_C, ~, ~, ~] = BesticDetector(img2, thresh_bestic, MaskMethod, SegmentMethod); 
        catch
            fprintf('Bestic could not be detected. Try adjusting the camera or tune the detection parameters. \n');
            fprintf('Exiting program...\n');
            A = false;
            break;
        end
        fprintf('Done.\n');

        fprintf('Locating Plate...');    
        %Calculates the center coordinates and radius of plate
        [centers2, radii2, metric2] = ACFPlateDetector(img_cropped, circle_range, thresh_plate, true); 
        fprintf('Done. (%1.1f s) \n', toc);

        try
            [M,L] = max(metric2); % Stores the row of "centers" that has the highest relative strength for circle center into var L
            centers2 = centers2(L,1:2); %Disregard the circle with lower relative strength.
            radii2 = radii2(L); % Same as above
        catch
            [centers2, radii2, metric2] = ACFPlateDetector(img_cropped, circle_range, thresh_plate, false);
            [M,L] = max(metric2); % Stores the row of "centers" that has the highest relative strength for circle center into var L
            centers2 = centers2(L,1:2); %Disregard the circle with lower relative strength.
            radii2 = radii2(L); % Same as above
        end

        % Secondary measurments
        dx2 = Bx2_C - centers2(1); % Horizontal dinstance between plate center and bestic
        dy2 = By2_C - centers2(2); % Vertical dinstance between plate center and bestic
        beta = atan(dy2/dx2); %Angle between plate center and Bestic display center point [degrees]

        zeta = angdiff(alpha,beta); %Angle difference between first and second measurment

        %Calculate translational difference between the two measurments
        D = [1 dx; 1 dx2; 1 dy; 1 dy2];
        %Preallocate
        out = zeros(size(D,1),1);
        index = D(:,1) == 1;

        %Calculate difference in matrix D
        xy = D(index,2);
        out(index) = [xy(2:end) - xy(1:end-1); xy(end) - xy(1)];

        %Check if the angular and translational offsets meet the
        %requirements for error signal
        if abs(out(1)) > trans_thresh || abs(out(3)) > trans_thresh || abs(zeta) > ang_thresh
            E = true;            
        else 
            E = false;
        end

        if E
            opts.Default = 'View Plot';
            opts.Interpreter = 'tex';
            ErrorQuestion = 'Bestic not aligned with plate. Recalibrate?';
            Choice = questdlg(ErrorQuestion, 'Please choose an option', 'Yes', 'End Program', 'View Plot',opts);
            switch Choice
                case 'Yes'  
                    State = 1;
                case 'End Program'
                    A = false;
                case 'View Plot'
                    State = 3;
            end
        else 
            i = 1; %Restart the snapshot loop
        end
        
        % End the snapshot loop if it has run more than the preset number
        % or if there's in input from the user to break.
        if RunCount > 1
            State = 3;
        end
        
    end
    
    %% View plot
    
    %Segment the last snapshot to keep part of the "original" tracking image 
    img2(1:yMax_C,round(centers(1)+radii + 25):xMax_C,1:3) = img(1:yMax_C,round(centers(1)+radii + 25):xMax_C,1:3);
    %Print rectangle around selected output object
    img2 = insertObjectAnnotation(img2,'rectangle',stats(col).BoundingBox,'','LineWidth',5,'Color','green','TextBoxOpacity',0);
    while State == 3 && A
            Size_cropped = size(img_cropped);
            figure();
            set(gcf,'Visible', 'off');  %Create figure but supress display
            imshow(img2, 'XData', 1:Size_cropped(2), 'YData', 1:Size_cropped(1)),title('Results');   
            hold on
            axis on; 
            
            %Print bounding boxes and image coordinates on plot
            for object = 1:length(stats)
                if object ~= col
                    bb = stats(object).BoundingBox;
                    rectangle('Position',bb,'EdgeColor','r','LineWidth',2)
                    plot(Tx_C,Ty_C, Bx_C,By_C, '-m+')
                    a=text(Tx_C(object),Ty_C(object), strcat('T',num2str(object),'= X: ', num2str(round(Tx_C(object))), '   Y: ', num2str(round(Ty_C(object)))));
                    set(a, 'FontName', 'Arial', 'FontSize', 10, 'Color', 'blue');   
                end
            end

            rectangle('Position',bb_B,'EdgeColor','r','LineWidth',2)
            b=text(Bx_C,By_C, strcat('BESTIC: X: ', num2str(round(Bx_C)), '   Y: ', num2str(round(By_C))));
            set(b, 'FontName', 'Arial', 'FontSize', 10, 'Color', 'g');
            theta_text=text(Bx_C,By_C + 50, strcat(' \theta:  ', num2str(theta*180/pi)));

            %Preallocating text variable c
            c = strings([1,object]);

            for i = 1:object %Loop for printing the Bestic coordinates on plot
                    c(i) = string(strcat(num2str(i),':POSITION: X = ', num2str(Q(1,i)), '    Y = ', num2str(Q(2,i))));
                    annotation('textbox',[0.07 0.21 0.1 0.1],'String',c,'FitBoxToText','on', 'BackgroundColor', 'white')
            end

            for i = 1:numel(Lines) %Draw the lines through the display area of Bestic
                    line(x_C,Lines(i).line)
            end
            
            %Show detected circle
            viscircles(centers,radii);

            set(gcf,'Visible', 'on'); %Show the resulting plot
            for n = 1:QSize(2)
            fprintf('Target %d: X: %1.1f  Y:%1.1f  Z:%1.1f\n',n,Q(1,n),Q(2,n),Q(3,n))
            end

           Choice = questdlg('Would you like to continue eating?', 'Yes', 'No');
        switch Choice
            case 'Yes'
                State = 1;
                print(sprintf('Plot_%04d.png', RunCount),'-dpng') %Saves plot to disk as .png
            case 'No'
                print(sprintf('Plot_%04d.png', RunCount),'-dpng') %Saves plot to disk as .png
                A = false;               
        end

        if strcmp(Choice,'Cancel')
            A = false;
        end

    hold off
    end   

end

%fclose(B); %Closes UART connection with Bestic

