function [] = RGBHist(img)    
%%
    %Get histValues for each channel
    [yRed, x] = imhist(img(:,:,1)); %Histogram of the red channel, Red = img(:,:,1);, Green = img(:,:,2) and so on
    [yGreen, x] = imhist(img(:,:,2));
    [yBlue, x] = imhist(img(:,:,3));

    %Plot them together in one plot
    plot(x, yRed, 'Red', x, yGreen, 'Green', x, yBlue, 'Blue');
end