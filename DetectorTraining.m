clc; clear;



%{
BesticPositiveInstances = load('labelingSession.mat'); %Loads the positive instances from the labeling session 

ImageFilename = BesticPositiveInstances.imageLabelingSession.ImageFilenames
IsChanged = BesticPositiveInstances.imageLabelingSession.IsChanged
IsPixelLabelChanged = BesticPositiveInstances.imageLabelingSession.IsPixelLabelChanged
FileName = BesticPositiveInstances.imageLabelingSession.FileName
PixelLabelDataPath = BesticPositiveInstances.imageLabelingSession.PixelLabelDataPath
ROILabelSet = BesticPositiveInstances.imageLabelingSession.ROILabelSet
ROISublabelSet = BesticPositiveInstances.imageLabelingSession.ROISublabelSet
ROIAttributeSet = BesticPositiveInstances.imageLabelingSession.ROIAttributeSet
FrameLabelSet = BesticPositiveInstances.imageLabelingSession.FrameLabelSet
ROIAnnotations = BesticPositiveInstances.imageLabelingSession.ROIAnnotations
FrameAnnotations = BesticPositiveInstances.imageLabelingSession.FrameAnnotations
HasROILabels = BesticPositiveInstances.imageLabelingSession.HasROILabels
NumROILabels = BesticPositiveInstances.imageLabelingSession.NumROILabels
HasFrameLabels = BesticPositiveInstances.imageLabelingSession.HasFrameLabels
NumFrameLabels = BesticPositiveInstances.imageLabelingSession.NumFrameLabels
NumROISublabels = BesticPositiveInstances.imageLabelingSession.NumROISublabels
NumAttributes = BesticPositiveInstances.imageLabelingSession.NumAttributes

positiveInstances.imageFilename = ImageFilename
%}
load('gTruth2_Bestic.mat')

trainingData = objectDetectorTrainingData(gTruth);
acfDetector = trainACFObjectDetector(trainingData,'NegativeSamplesFactor', 9, 'NumStages', 16, 'MaxWeakLearners', 4096);

%%
clear; clc;


cam = webcam(1); %Initialize camera
cam.Resolution = char(cam.AvailableResolutions(3)); %Set resolution to 1920x1080
load('cameraParamsR200.mat'); %Loads calibration data for R200 camera
warning('off', 'Images:initSize:adjustingMag'); %Disable warning for image too big
pause(2); %Camera need a second to adjust its parameters before it's functional
img = zeros(1920,1080,3); %Preallocate
%%
clc;
load('ACF_BesticDetector4.mat')
Frames = 3;
MaxSize = [720 720];
MinSize = [120 160];
figure;
for i = 1:Frames

img = snapshot(cam);
img = undistortImage(img, cameraParams); % Corrects for lens distortion
img = NoiseRM(img); %Removes color noise from image
[bboxes, scores] = detect(acfDetector,img,'Threshold',-9, 'MaxSize',MaxSize, 'MinSize',MinSize); %measurement outputs for bboxes are x,y,width,height

[~,idx] = max(scores);
Strongest = bboxes(idx,:);

annotation = sprintf('Confidence = %.1f',max(scores));
img = insertObjectAnnotation(img,'rectangle',Strongest,annotation);
imshow(img)
end
%%
for i = 1:length(scores)
   annotation = sprintf('Confidence = %.1f',scores(i));
   img = insertObjectAnnotation(img,'rectangle',bboxes(i,:),annotation);
end

figure
imshow(img)