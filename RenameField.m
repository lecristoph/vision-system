function S = RenameField(S, Old, New)

Data  = struct2cell(S);
Field = fieldnames(S);
Field{strcmp(Field, Old)} = New;
S     = cell2struct(Data, Field);

return;
end