function [centers, radii] = FindCircle(img)

[centers, radii] = imfindcircles(img,[190 220],'ObjectPolarity','dark','Sensitivity',0.97, 'EdgeThreshold', 0.03,'Method','twostage');
imshow(img);

h = viscircles(centers,radii);

end