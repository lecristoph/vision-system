%
% Author:
% Christoffer Karlsson, for Camanio Care.
% Electrical Engineering @
% Karlstad University, 2018.
% email: christoffer.karlsson@europe.com
%

function [centers, radii, metric] = ACFPlateDetector(img, circle_range, thresh_plate, CF, SE)

if nargin == 3
    CF = true;
    SE = true;
elseif nargin == 4
    SE = true;
end

%%
Detector = load('ACF_PlateDetector2.mat');

%Check the general luminance of input image
%If it's too low, increase the brightness
lum = mean2(rgb2gray(img));
if lum < 120
    img = AutoEnhance(img,'HSV',1.4,'Contrast');
end


%Runs ACF detection algorithm and outputs bounding box coordinates and a
%confidence score.
%measurement outputs for bboxes are [x,y,width,height]
%a higher "score" is better
[bboxes, scores] = detect(Detector.acfDetector,img,'Threshold',thresh_plate,'MaxSize',[1100 1080],'MinSize',[500 500]); 

[m,i] = max(scores); %Determine what row in "scores" has the highest value (places in variable i, m is value)
if abs(m) > 0
    figure('Visible','off') 
    imshow(img); %Display original image
    R = imrect(gca,bboxes(i,:)+[0 0 0.05*bboxes(3) 0.05*bboxes(4)]); %Create ROI area from detected bboxes
    Mask = R.createMask(); %Creates a mask around ROI
    maskedImage = img.*cast(Mask, class(img)); %Converts the Mask to same class as img(.png) and multiplies them together
    M = repmat(all(~maskedImage,3),[1 1 3]); %mask black parts
    maskedImage(M) = 255; %turn the black parts white
    maskedImage = imadjust(maskedImage,[0.3 0.8],[0.2 1]); %increase contrast
    %%
    if ~CF
        % Extract the red channel of the masked image
        redChannel = maskedImage(:, :, 1);
        binaryImage1 = redChannel < 120;

        % Fill holes.
        binaryImage = imfill(binaryImage1, 'holes');
        % Get rid of small blobs.
        binaryImage = bwareaopen(binaryImage, 4000);
        % Smooth border
        binaryImage = imclose(binaryImage, true(5));

        %Find perimeter by calculating the gradient of the image
        BW = imgradient(binaryImage);
        %BW = edge(BW,'Canny',0.15);

        %[centers, radii, metric] = imfindcircles(BW,circle_range,'ObjectPolarity','bright','Sensitivity',0.99, 'EdgeThreshold', 0.001);
        [centers, radii, metric] = imfindcircles(BW,circle_range,'ObjectPolarity','bright','Sensitivity',0.991, 'EdgeThreshold', 0.001,'Method','twostage');
        
    else       
        AInv = imcomplement(maskedImage);
        BInv = imreducehaze(AInv, 'Method','approx','ContrastEnhancement','boost');
        maskedImage = imguidedfilter(BInv);
        % Extract the blue channel 
        im_gray = maskedImage(:, :, 3);

        %Increase contrast
        im_gray = imadjust(im_gray,[0 0.7],[]);
        im_gray = adapthisteq(im_gray);

        % Use a median filter to filter out noise
        im_gray = medfilt2(im_gray, [5 5]);
        
        if ~SE
            % Convert the resulting grayscale image into a binary image.
            bw = imbinarize(im_gray,'adaptive','ForegroundPolarity','dark','Sensitivity',0.49);
            % Invert it
            bw = imcomplement(bw);

            LL = round((bboxes(3)*bboxes(4))*0.02); % 2 percent of total bb
            UL = round((LL/0.02)*0.95); % 90 percent of total bb

            % Remove all those pixels outside limit of LL and UL
            bw = xor(bwareaopen(bw,LL),  bwareaopen(bw,UL*40));

            % Smooth border
            bw = imclose(bw, true(5));
        else
            se = strel('disk',8); %Create a disk shaped structuring element with radius 8.  
            nhood = getnhood(se); %Get the neighborhood of morphological structuring element

            J = rangefilt(im_gray,nhood); %Perform range filtering
            J = medfilt2(J, [5 5]); %Filter out noise with median filter
            J = AutoEnhance(J,'RGB',25); %increase brightness slightly

            bw = J > 76; %Turn into binary by threshholding. Change 72 to something larger/smaller if you encounter faults...
            bw = bwpropfilt(bw,'MajorAxisLength',8); %Filter out tiny objects�
            bw = bwpropfilt(bw,'Eccentricity',2,'smallest'); %Filter out straight lines
            bw = imfill(bw,'holes'); %Fill in holes in binary image

            %bw = imerode(bw,nhood); %Erode the image in regions outside the neighborhood of the SE.
            bw = imclose(bw, true(5)); %Clean up the borders
            bw = bwareaopen(bw,5000); %Clean up remaining spots           
        end

        stats = regionprops(bw, 'MajorAxisLength','BoundingBox', 'PixelList','ConvexArea','ConvexImage');
        
        W = zeros(1,numel(stats)); %Preallocate W
        for i = 1:numel(stats)
            W(i) = stats(i).MajorAxisLength; % Struct into array
        end
        [~,L] = max(W); %The row with highest major axis length
        
        
        % Remove warnings if circle_range > 100
        warning('off','images:imfindcircles:warnForLargeRadiusRange')
        %Detect using Circular Hough Transform
        [centers, radii, metric] = imfindcircles(bw,circle_range,'ObjectPolarity','bright','Sensitivity',0.991, 'EdgeThreshold', 0.001,'Method','twostage');
        if ~isempty(metric)
            [~,L] = max(metric); % Stores the row of "centers" that has the highest relative strength for circle center into var L
            centers = centers(L,1:2); %Disregard the circle with lower relative strength.
            radii = radii(L); % Same as above
        end
        
        %If Hough transform fails, use Circle fit by pixels instead.
        if ~(round(radii) >= circle_range(1) && round(radii) <= circle_range(2)) || isempty(metric)        
            %Fit circle to pixels
            Par = CircleFit(stats(L).PixelList);
            if Par(3) >= circle_range(1) && Par(3) <= circle_range(2)
                centers = Par(1:2);
                radii = Par(3);
                metric = 1;
            else            
                %Preallocate an empty struct
                stats2 = struct('Eccentricity',cell(numel(stats),1));
                %Get regionprops for each convex image
                for i = 1:numel(stats)
                    stats2(i) = regionprops(stats(i).ConvexImage,'Eccentricity');
                end

                W = zeros(1,numel(stats)); %Clear old values    
                for i = 1:numel(stats)
                  W(i) = stats2(i).Eccentricity; % Struct into array
                end
                [~,L] = min(W); %The row for the shape most resembling a circle

                Par = CircleFit(stats(L).PixelList); %Fit circle to pixels
                centers = Par(1:2);
                radii = Par(3);
                metric = 1;                       
            end
        end        
    end
    
close all
else
    disp('Could not find plate')
       centers = [];
       radii = [];
       metric = [];  
end

end

