%Function that decides from what direction to grasp the object.
% Grasping direction is determined by the targets proximity to the plate
% perimeter.
% Grasping distance is determined by the distance between target
% and the proximity to either the plate perimeter or another nearby
% object on the plate.
function [QNew] = AttackDir(Q)
%%
%Angle for rotating the coordinate system
Psi = pi/2;
%Rotation matrix around Z-axis
RotZ = [cos(Psi), -sin(Psi), 0; sin(Psi), cos(Psi), 0; ...   
           0, 0, 1]; 
% Perform rotation by transformation       
QNew = RotZ'*Q;

%Preallocate variables in loop
h = zeros(1,length(QNew));
psi = zeros(1,length(QNew));

%Calculate angle from x-axis
for i = 1:length(QNew)
    h(i) = sqrt(QNew(1,i)^2+QNew(2,i)^2);
    psi(i) = asind(QNew(2,i)/h(i));    
    if psi(i) > 0
    QNew(4,i) = 1; % Grasp the target from the right
    else
    QNew(4,i) = 0; % Grasp the target from the left
    end 
end
%Convert back to original coordinate system
QSize = size(Q);
for i = 1:QSize(1)
    QNew(i,:) = Q(i,:);
end


end