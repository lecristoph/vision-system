function Qout = Convert2Char(Q)

    %--- Set lower and upper limit for plate radius (Bestic plate has inner
    %radius 18.85cm)--- %
    RL = -18.85/2; %-PlateDiam/20, denumerator is 20 because 0.5*(1/10), convert from mm to cm 
    RU = 18.85/2;  %PlateDiam/20

    %--- Set scaling values for min/max ---%
    XYscalingMin = -180; %Minimum scaling value in decimal
    XYscalingMax = 180; %Maximum scaling value in decimal

    %--- Create matrix with the lowest/highest allowed coordinates added as
    %first and last element ---%
    QE = zeros(3,length(Q)+2); %Preallocate and make room for two extra elements
    for k = 1:3
        QE(k,1) = RL; %Add in the lower element
        QE(k,2:length(Q)+1) = Q(k,:); %Add in the elements from the Q-matrix
        QE(k,length(Q)+2) = RU; %Add in the upper element
    end

    %--- Perform scaling of matrix --- %
    Qout = QE(1:3,:) - min(QE(:)); %Subtract the minimum value
    %Divide the subtracted value with the range of values in Qout (complete
    %dimension), then multiply with the range of the scaling values
    Qout = (Qout/range(Qout(:)))*(XYscalingMax-XYscalingMin); 
    %Add the lower scaling value to each element
    Qout = Qout + XYscalingMin;

    %---Remove the upper and lower scaling values---%
    LA(1,1:length(Qout)) = 0; %Create a 1xn array of zeros
    LA(1,2:length(Q)+1) = 1; % Set the elements that correspond to actual objects to 1
    % Convert to logical array, where true values indicates elements to keep
    LA = logical(LA);
    % Remove elements in Qout based on logical index in matrix LA and round to
    % nearest integer
    Qout = round(Qout(:,LA));
    %Put back the rows for grasping direction and spoon travel distance
    Qout(4:5,:) = Q(4:5,:); 
    
    %--- Convert to ACII (char) ---%

end



        
