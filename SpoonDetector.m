%
% Author:
% Christoffer Karlsson, for Camanio Care.
% Electrical Engineering @
% Karlstad University, 2018.
% email: christoffer.karlsson@europe.com
%

function [I2, maskedImage] = SpoonDetector(img, centers, radii, thresh_spoon, ScoreThresh)

if nargin == 3
    thresh_spoon = -1; %Default threshhold for acf detection
    ScoreThresh = 60; %Default limit for "Spoon detected"
elseif nargin == 4
    ScoreThresh = 60; 
end

%%
% Constants
CircleMarg = 65; %Margin offset outside plate radius [px]
L_Limit = 5000; %Lower limit
U_Limit = 10^6; %Upper limit
MaxSize = [450 450]; % Maximum detection area
MinSize = [100 100]; % Minimum detection area

img_M2 = Mask_plate(img,centers,radii+CircleMarg); %Crop with a small margin to only show plate

load('ACF_SpoonDetector1.mat') %Load in detector, gTruth 

%Detect spoon in cropped image
[bboxes, scores] = detect(acfDetector,img_M2,'Threshold',thresh_spoon, 'MaxSize',MaxSize, 'MinSize',MinSize); %measurement outputs for bboxes are x,y,width,height

%Get the bbox with the strongest confidence
[S,idx] = max(scores); 

%%
%Chech if the spoon was detected
if  ~isempty(idx) && S > ScoreThresh 
    fprintf('Spoon detected, removing spoon from tracking area...');
    Strongest = bboxes(idx,:);
    for i = 3:4
       Strongest(i) = Strongest(i)*1.1; %Increase size of bounding box for some extra margin
    end

    %Show the image with bounding box included
    %annotation = sprintf('Confidence = %.1f',max(scores));
    %img = insertObjectAnnotation(img,'rectangle',Strongest,annotation);

    %Crop out the "spoonregion" of the image
    figure('Visible','off') 
    imshow(img_M2); %Display original image
    R = imrect(gca,Strongest); %Create ROI area from detected bboxes
    Mask = R.createMask(); %Creates a mask around ROI
    maskedImage = img_M2.*cast(Mask, class(img_M2)); %Converts the Mask to same class as img(.png) and multiplies them together
    M = repmat(all(~maskedImage,3),[1 1 3]); %mask black parts
    maskedImage(M) = 255; %turn the black parts white
    maskedImage = imadjust(maskedImage,[0.3 0.75],[0.2 1]); %increase contrast

%% Locate spoon position

    BW = imcomplement(im2bw(rgb2gray(maskedImage))); %Create binary image 
    BW = xor(bwareaopen(BW,L_Limit),  bwareaopen(BW,U_Limit)); %Remove very small or very large objects
    stats = regionprops(BW, 'Area','Centroid' ,'BoundingBox'); % Get the properties of the logical regions
%%
    %Check if there are more large object in the binary image and save only the
    %largest one
    Objects = bwconncomp(BW);
    if Objects.NumObjects > 1    
            %Preallocate A
        A = zeros(numel(stats),1);

        for i = 1:numel(stats) %Create array from struct field 'Area'
            A(i,:) = stats(i).Area;
        end
        [~,idx] = max(A); %Get the line with largest area
        NewStats.Centroid = stats(idx).Centroid;
        NewStats.BoundingBox = stats(idx).BoundingBox;
        clear stats; %Clear old values
        stats = NewStats;
    end  
 %%   Create ROI around spoon
 
    %CREATE IMAGE WHERE SPOON IS REMOVED    
        % Mask the tang of the spoon in white:       
        Y = stats.Centroid-stats.BoundingBox(4)/2;
        X = stats.Centroid-stats.BoundingBox(3)/2 + stats.BoundingBox(3)/2;
        h1=imrect(gca,[X(1)-120,Y(2)-200,130, 220]);
        Mask1 = h1.createMask(); %Creates a mask around ROI
        I2 = img.*cast(~Mask1,class(img)); % multiplies
        M1 = repmat(all(~I2,3),[1 1 3]); %mask black parts
        I2(M1) = 255; %turns them white

        %Mask the elliptical area of the spoon in white:
        h = imellipse(gca,[stats.Centroid-[stats.BoundingBox(3)/2 stats.BoundingBox(4)/2] stats.BoundingBox(3) stats.BoundingBox(4)]);
        Mask2 = h.createMask(); %Creates a mask around ROI
        I2 = I2.*cast(~Mask2,class(img)); % multiplies
        M = repmat(all(~I2,3),[1 1 3]); %mask black parts
        I2(M) = 255; %turns them white

    fprintf('Done. \n');
   
else
    I2 = img;
    maskedImage = '';
end
