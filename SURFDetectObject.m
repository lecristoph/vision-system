%Recognision of Bestic
function [X,Y] = SURFDetectObject(ref_img, img, index_thresh)

ref_img = rgb2gray(ref_img); %Converts the reference image to grayscale
ref_pts = detectSURFFeatures(ref_img, 'MetricThreshold', 700); %Computes the SURFFeature-points of the reference img
[ref_features,  ref_validPts] = extractFeatures(ref_img,  ref_pts); %Extracts them

img = rgb2gray(img); %Converts the image to be evaluated to grayscale

%Detects features of snapshot
img_pts = detectSURFFeatures(img, 'MetricThreshold', 700);
[img_features, img_validPts] = extractFeatures(img, img_pts);


%Compare features of reference with features of snapshot
index_pairs = matchFeatures(ref_features, img_features);

ref_matched_pts = ref_validPts(index_pairs(:,1)).Location;
img_matched_pts = img_validPts(index_pairs(:,2)).Location;

figure;
showMatchedFeatures(ref_img, img, ref_matched_pts,img_matched_pts)

%If there are sufficient amount of matched points, proceed to calculate
%coordinates
if sum(index_pairs) > index_thresh

%Calculates the centerpoint of the Destination pattern
minC = min(double(img_matched_pts)); %Calculates the lowest value of xy coordinates from the target
maxC = max(double(img_matched_pts)); % -||- highest value

[X, Y] = double((minC + maxC)/2); %Takes the mean value of max and min. This is the return of the function.

else msgbox('No matches')
end

end