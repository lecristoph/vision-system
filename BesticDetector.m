% This function takes inputs:
% image file, threshhold value, Masking Method and Segmentation method.
% image file: should be an rgb image
% threshold: float or integer.
% Masking Method: 'Fill'(default) or 'Subtract'
% Segmentation Method: 'Shape' (default)  or 'Blob'
%
% The function will return an X,Y position of the display area on Bestic,
% as well as the corresponding boundingbox, angle relative positive X-axis
% and the line functions which are only used for plotting the result.
%
% Example: [X, Y, BB, theta, f] = BesticDetector(img, -5, 'Fill', 'Shape')
%
% Author:
% Christoffer Karlsson, for Camanio Care.
% Electrical Engineering @
% Karlstad University, 2018.
% email: christoffer.karlsson@europe.com
%

function [X, Y, BB, theta, f] = BesticDetector(img, thresh_bestic, MaskMethod, SegmentMethod)

if nargin == 2
    MaskMethod = 'Fill';
    SegmentMethod = 'Shape';
elseif nargin == 3
    SegmentMethod = 'Shape';
end
%%
Detector = load('ACF_BesticDetector4.mat'); %Loads the ACF Detector for Bestic detection

LL = 3000; % Area tracking limit [px^2]
UL = 45000; % Display is usually around 6000-20000 px^2 depending on camera height etc
x_img = 0:1:length(img);
MaxSize = [800 800];
MinSize = [120 160];

%Runs ACF detection algorithm and outputs bounding box coordinates and a
%confidence score.
%measurement outputs for bboxes are [x,y,width,height]
%a higher "score" is better

[bboxes, scores] = detect(Detector.acfDetector,img,'Threshold',thresh_bestic, 'MaxSize',MaxSize, 'MinSize',MinSize);
%Get the bbox with the strongest confidence score
[~,idx] = max(scores);

    %%
if ~isempty(idx) 
    figure('Visible','off') 
    h = imshow(img); %Display original image
    R = imrect(gca,bboxes(idx,:)); %Create ROI for Bestic area from detected bboxes
    Mask = R.createMask(h); %Creates a rectangular (white box) mask around ROI

    maskedImage = img.*cast(Mask, class(img)); %Converts the Mask to same class as img(.png) and multiplies them together
    M = repmat(all(~maskedImage,3),[1 1 3]); %mask black parts
    maskedImage(M) = 255; %turn the black parts whites

    maskedImage = imadjust(maskedImage, [0.2, 0.9],[]); %increase contrast
    % Display image with the ROI boxed in.
    figure('Visible','off') 
    imshow(maskedImage);   

    M = repmat(all(~maskedImage,3),[1 1 1]); %create a red mask
    maskedImage(M) = 255; %Turn the black parts red
    
    if strcmp(MaskMethod,'Subtract')
        %Extract the red parts of the image
        diff_red = imsubtract(maskedImage(:,:,1), rgb2gray(maskedImage)); 

        %Use a median filter to filter out noise
        diff_red = medfilt2(diff_red, [5 5]);
        %Fill in area with holes
        diff_red = imfill(diff_red,8,'holes'); 
        % Convert the resulting grayscale image into a binary image.
        bw = imbinarize(diff_red,'adaptive','ForegroundPolarity','bright','Sensitivity',0.001);

        % Keep only connected areas within a certain size
        bw = xor(bwareaopen(bw,LL),  bwareaopen(bw,UL));

        % Convert to logical image
        bw = logical(bw);
        

    elseif strcmp(MaskMethod,'Fill')           
        % Convert to grayscale, adjust contrast and create a mask
        mask = adapthisteq(rgb2gray(maskedImage));

        % Create a marker image that correspond to high intensity objects
        % (morphological erosion)
        se = strel('disk',5);
        marker = imerode(mask,se);

        %increase highlights
        marker = imadjust(marker,[0,0.7],[]);
        %Remove low contrast areas and make logical
        bw = ~(marker < 128);
        % Convert to binary image
        %bw = imbinarize(marker,'adaptive','ForegroundPolarity','dark','Sensitivity',0.001);

        % Fill holes between sections
        bw = imfill(~bw,4,'holes');

        % Remove very large and very small objects
        bw = imcomplement(xor(bwareaopen(bw,LL),  bwareaopen(bw,UL)));
%%
    end
     
    if strcmp(SegmentMethod,'Blob') || strcmp(SegmentMethod,'blob')
        
        % Image blob analysis.
        % Get a set of properties for each labeled region.
        stats = regionprops(bw, 'Area','Centroid','BoundingBox','Eccentricity', 'Extent');
        
        %Preallocate A
        A = zeros(numel(stats),1);
        hold on

        for i = 1:numel(stats) %Create array from struct
            A(i,:) = stats(i).Extent;
        end

        try
            [~, L]= max(A); %Get the field that has the biggest area (stored in L)
            S = stats(L).Centroid; %center coordinates of the display on Bestic
            X = S(1); %X-center coordinate of display
            Y = S(2); %Y-center coordinate of display
            BB = stats(L).BoundingBox; %The boundingbox around the display
            close all

        catch 
            %Try again using 'Subtract' MaskMethod and another ACF-detector
            %(trained in 24 steps with 8*Negative factor)
            MaskMethod = 'Subtract';
            load('ACF_BesticDetector3.mat'); %Loads the ACF Detector for Bestic detection
            [bboxes, scores] = detect(acfDetector,img,'Threshold',-11);
            [~,idx] = max(scores); %Determine what row in "scores" has the highest value (places in variable i, m is value)

            figure('Visible','off') 
            h = imshow(img); %Display original image
            R = imrect(gca,bboxes(idx,:)); %Create ROI for Bestic area from detected bboxes
            Mask = R.createMask(h); %Creates a mask around ROI

            maskedImage = img.*cast(Mask, class(img)); %Converts the Mask to same class as img(.png) and multiplies them together
            M = repmat(all(~maskedImage,3),[1 1 3]); %mask black parts
            maskedImage(M) = 255; %turn the black parts whites

            maskedImage = imadjust(maskedImage, [0.2, 0.9],[]); %increase contrast
            % Display image with the ROI boxed in.
            figure('Visible','off') 
            imshow(maskedImage);   

            M = repmat(all(~maskedImage,3),[1 1 1]); %create a red mask
            maskedImage(M) = 255; %Turn the black parts red

            %Extract the red parts of the image
            diff_red = imsubtract(maskedImage(:,:,1), rgb2gray(maskedImage)); 

            %Use a median filter to filter out noise
            diff_red = medfilt2(diff_red, [5 5]);

            bw = im2bw(diff_red);

            % Keep only connected areas within a certain size
            bw = xor(bwareaopen(bw,LL),  bwareaopen(bw,UL));

            % Convert to logical image
            bw = logical(bw);

            % Image blob analysis.
            % Get a set of properties for each labeled region.
            stats = regionprops(bw, 'Area','Centroid','BoundingBox','Eccentricity', 'Extent');
            %Preallocate A
            A = zeros(numel(stats),1);

            for i = 1:numel(stats) %Create array from struct (Eccentricity)
                A(i,:) = stats(i).Eccentricity;
            end

            [~, idx]= max(A); %Get the field that has the biggest area (stored in L)
            S = stats(idx).Centroid; %center coordinates of the display on Bestic
            X = S(1); %X-center coordinate of display
            Y = S(2); %Y-center coordinate of display
            BB = stats(L).BoundingBox + [0 0 150 40]; %The boundingbox around the display
            close all
        end
        
    elseif strcmp(SegmentMethod,'Shape') || strcmp(SegmentMethod,'shape')
        
        [B,C,N] = bwboundaries(~bw,'noholes');

        %get stats       
        stats = regionprops(C,'Area','Centroid','BoundingBox', 'Perimeter');
        
        Perimeter = cat(1,stats.Perimeter);
        Area = cat(1,stats.Area);
        
        %Define metrics
        CircleMetric = (Perimeter.^2)./(4*pi*Area); 
        SquareMetric = NaN(N,1);
        TriangleMetric = NaN(N,1);
        
        %for each boundary, fit to bounding box, and calculate some parameters
        for k=1:N
           boundary = B{k};
           [rx,ry,boxArea] = minboundrect(boundary(:,2), boundary(:,1));  %x and y are flipped in images
           %get width and height of bounding box
           width = sqrt( sum( (rx(2)-rx(1)).^2 + (ry(2)-ry(1)).^2));
           height = sqrt( sum( (rx(2)-rx(3)).^2+ (ry(2)-ry(3)).^2));
           aspectRatio = width/height;
           if aspectRatio > 1  
               aspectRatio = height/width;  %make aspect ratio less than unity
           end
           SquareMetric(k) = aspectRatio;    %aspect ratio of box sides
           TriangleMetric(k) = Area(k)/boxArea;  %filled area vs box area
        end
        
        %Rectangular shapes have these properties
        isRectangle = ~(CircleMetric < 1.1) & ~(TriangleMetric < 0.6) & ~(SquareMetric > 0.9); 
        
        %Preallocate matrix RC
        RC = zeros(numel(stats),2);
        %Get the coordinates of the rectangle centroid
        for i = 1:numel(stats)
            if isRectangle(i)
                RC(i,:) = stats(i).Centroid; %Store coordinates of rectangle
            end
        end
        
        %If there are more elements in RC > 0, get only the largest
        %rectangle.
        D = zeros(length(RC),1);
        for i = 1:length(RC)
            if RC(i) ~= [0 0]
                D(i) = stats(i).Area;
            else
                D(i) = 0;
            end
        end
        D = transpose(D);

        [~, Row] = max(D);
        S = stats(Row).Centroid;
        X = S(1); %X-center coordinate of display
        Y = S(2); %Y-center coordinate of display
        BB = stats(Row).BoundingBox; %The boundingbox around the display
        
   
    end
        %% Corrects if more than one blob is visible in bw

        BB_Margin = [-20 -20 30 30]; % [X Y Width Height], is used to ensure that the whole display is visible
        
        if ~strcmp(MaskMethod,'Fill')
            bw = imcomplement(bw); %Convert back to the non-inverted version           
        end
        
        figure('Visible','off') 
        h_bw = imshow(bw); %Create handle for image
        R2 = imrect(gca,BB + BB_Margin ); %Create ROI for Bestic area from detected bboxes
        Mask2 = R2.createMask(h_bw); %Set mask around the bounding box for the display part on Bestic

        Bestic_Display = bw.*cast(Mask2, class(bw)); %Converts the Mask to class .png and multiply with diff_im
        Bestic_Display(~Mask2) = 255; %turn the black parts whites

        Bestic_Display  = imadjust(Bestic_Display,[0.1 0.9],[]); %remove distortion from segmenting

        %%
        %Find angle theta of the line segment (display on Bestic), relative to
        %camera xy-axis:
        %BW = edge(Bestic_Display,'Roberts', 0.001); % edge filter 
        BW = imgradient(Bestic_Display); %Calculates the image gradient (directional change in the intensity)

        %Create Hough transform
        [H,T,R] = hough(BW);

        %Find peaks in the Hough transform
        P  = houghpeaks(H,5,'threshold',ceil(0.01*max(H(:))));
        
        %Fillgap = disregard lines with less than _px between them
        lines = houghlines(BW,T,R,P,'FillGap',5,'MinLength',60); 

        %% Calculate the slope (k) and angle from the image x-axis for each line segment
        %Preallocate k,f,Theta
        k = zeros(1,numel(lines));
        f = struct('line',zeros(1,numel(lines)));
        Theta = zeros(1,numel(lines));

        for i = 1:numel(lines) 
        k(i) = (lines(i).point2(2) - lines(i).point1(2)) / (lines(i).point2(1) - lines(i).point1(1)); %k = y2-y1/x2-x1 (derivative)
        %create one point formula line function of each detected line segment
        f(i).line = k(i).*(x_img - lines(i).point1(1))+lines(i).point1(2); %f = k(x-x0)+y0, stored in struct, f.line
        Theta(i) = atand((f(i).line(length(f(i).line)) - f(i).line(1)) / length(f(i).line)); % 
        end

        theta = -round(mean(Theta*pi/180), 2); % Takes the mean value and rounds of to two decimal places and converts to radians
                                               % If Bestic is situaded to the left
                                               % of the plate, remove the minus
                                               % sign before rounding
 %% If all goes well, this section will never run
 
        if ~isfinite(theta) % If theta could not be computed, take the angle from hough lines instead
          for i = 1:numel(lines) % Put struct lines.theta into array
              B(i) = lines(i).theta;
          end
          theta = -round(mean(B)-90,2); % Take mean value of all angles and rotate 90 deg
        end
                                              
        close all
        
else
    disp('Could not find Bestic')
end
end