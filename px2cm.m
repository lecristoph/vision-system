function [D,BBB] = px2cm(img_cropped,Plot)
%%
if nargin == 1
    Plot = false;
end
%%
    LL = 300;
    UL = 10^5;
    dist_cm = 12.75; %fixed distance in cm between circular blobs, measured IRL
    threshold = 0.87; %threshhold for circular object
    
    % Convert to grayscale, adjust contrast and create a mask
    mask = adapthisteq(rgb2gray(img_cropped));

    % Create a marker image that correspond to high intensity objects
    % (morphological erosion)
    se = strel('disk',5);
    marker = imerode(mask,se);

    %increase highlights
    marker = imadjust(marker,[0,0.4],[]);
    %Remove low contrast areas and make logical
    bw = ~(marker < 128);

    % Fill holes between sections
    bw = imfill(~bw,4,'holes');

    % Remove very large and very small objects
    bw = xor(bwareaopen(bw,LL),  bwareaopen(bw,UL));

    % Find bondaries around objects
    [B,L] = bwboundaries(bw,'noholes');

    if Plot
        % Display the label matrix and draw each boundary 
        figure;
        imshow(label2rgb(L, @jet, [.5 .5 .5]))  
        hold on
    end
    for k = 1:length(B)
    boundary = B{k};
        if Plot
            plot(boundary(:,2), boundary(:,1), 'w', 'LineWidth', 2)
        end
    end

    % Get the properties of the objects
    stats = regionprops(L,'Area','Centroid','BoundingBox');

    %Preallocate C
    C = struct('centroid',cell(1,length(B))); 
    BB = struct('centroid',cell(1,length(B)));
    % loop over the boundaries
    for k = 1:length(B)
        % obtain (X,Y) boundary coordinates corresponding to label 'k'
        boundary = B{k};

        % compute a simple estimate of the object's perimeter
        delta_sq = diff(boundary).^2;    
        perimeter = sum(sqrt(sum(delta_sq,2)));

        % obtain the area calculation corresponding to label 'k'
        area = stats(k).Area;

        % compute the roundness metric
        metric = 4*pi*area/perimeter^2;

        % display the results
        metric_string = sprintf('%2.2f',metric);

        % mark objects above the threshold with a black circle
        if metric > threshold
        C(k).centroid = stats(k).Centroid;
        BB(k).BoundingBox = stats(k).BoundingBox;
        end
        if Plot
            text(boundary(1,2)-35,boundary(1,1)+13,metric_string,'Color','y',...
            'FontSize',14,'FontWeight','bold');
        end
    end

    R = zeros(numel(C),2);
    BBB = zeros(numel(C),2);
    for k = 1:numel(C)
        if ~isempty(C(k).centroid)
            R(k,1:2) = C(k).centroid;
            BBB(k,1:4) = BB(k).BoundingBox;
        end
    end
    %Remove zero rows
    R = R(any(R,2),:); %Centroid coordinates 
    BBB = BBB(any(BBB,2),:); %Bounding Boxes of Blobs

    dist_px = abs(R(1)-R(2));
    D = dist_px / dist_cm; %D is the pixel value for one centimeter

end


        