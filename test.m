
Rmax = 9;
 for k = 1:length(stats)
            XYMax(k) = sqrt(QNew(1,k)^2 + QNew(2,k)^2); %Calculate maximum XY-coordinates
            if QNew(4,k) == 0 %Grasp from the left
               XNew(1,k) = QNew(1,k) - bb(k,3)/(2*px); %X coordinate, grasp from middle of left bb
               C2bbDist(1,k) = abs(abs(XNew(1,k))-abs(QNew(1,k))); %Distance from center coord to bb edge
            else
               XNew(1,k) = QNew(1,k) + bb(k,3)/(2*px); %X coordinate, grasp from middle of right bb
               C2bbDist(1,k) = abs(abs(XNew(1,k))+abs(QNew(1,k))); %Distance from center coord to bb edge
            end
            
            % Limit coordinates to inside plate radius
            if XNew(1,k) > XYMax(k)
                XNew(1,k) = XYMax;
            elseif XNew(1,k) < -XYMax(k)
                XNew(1,k) = -XYMax(k);
            end
 end

 for k = 1:length(stats)
 Xmax(k) = sqrt(Rmax^2-QNew(2,k)^2)
 end