clear; clc; close all

cam = webcam(1); %Initialize camera
cam.Resolution = char(cam.AvailableResolutions(3)); %Set resolution to 1920x1080
preview(cam)
%%
savepath = 'ref';  %where the files will be saved
ReferenceNameTemplate = 'bestic_%d.png';  %name pattern
%%
Frames = 210; %Number of iterations (total amount of snapshots to be processed)

for n = 196:Frames
   
   img = snapshot(cam);
   if strcmp(cam.Name,'Intel(R) RealSense(TM) 415 RGB') && mod(n,2) == 1
     img = flip(img,2); %Horizontal flip
     img = flip(img,1); %Vertical flip
   end
   
      %Write image to file
      thisfile = sprintf(ReferenceNameTemplate, n);  %create filename
      fullname = fullfile(savepath, thisfile);  %folder and all
      imwrite(img, fullname);  %saves the image as png
     
            pause(1); 
      disp('1...')

            pause(4); 
      disp('Click!')
      disp(n)

      
end