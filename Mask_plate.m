function [maskedImage] = Mask_plate(PlateImage, centers, radii)


%%

% Get the dimensions of the image.  numberOfColorBands should be = 1.
[rows, columns, numberOfColorBands] = size(PlateImage);
% Initialize parameters for the circle
circleCenterX = centers(1); 
circleCenterY =  centers(2); % pixels from the top left corner 
circleRadius = radii(1);    % circle radius 

% Initialize an image to a logical image of the circle. 
circleImage = false(rows, columns); 
[x, y] = meshgrid(1:columns, 1:rows); 
circleImage((-x + circleCenterX).^2 + (-y + circleCenterY).^2 <= circleRadius.^2) = true; 

% Mask the image with the circle.
if numberOfColorBands == 1
	maskedImage = PlateImage; % Initialize with the entire image.
	maskedImage(~circleImage) = 1; % Zero image outside the circle mask.
else
	% Mask the image.
	maskedImage = bsxfun(@times, PlateImage, cast(circleImage,class(PlateImage)));
end
M = repmat(all(~maskedImage,3),[1 1 3]); %mask black parts
maskedImage(M) = 255; %turn them white

%imshow(maskedImage); 

%disp(centers)
%disp(radii)


end