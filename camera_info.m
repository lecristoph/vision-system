%info = imaqhwinfo('winvideo', 3)
%obj = videoinput('winvideo', 3)
clc;
im = snapshot(cam);
imSize = size(im);
im = imresize(im, imSize(1:2)*0.5);

if strcmp(cam.Name,'Intel(R) RealSense(TM) 415 RGB')
    im = flip(im,2); %Horizontal flip
    im = flip(im,1); %Vertical flip
end

[imagePoints, boardSize]=detectCheckerboardPoints(im);
squareSize=21; % in millimeters
worldPoints=generateCheckerboardPoints(boardSize, squareSize);

[R, t]=extrinsics(imagePoints, worldPoints, cameraParams);
T = [R(1, :); R(2, :); t]*cameraParams.IntrinsicMatrix;

tform=invert(projective2d(T));

%tform.T = [1 0 -0; 0 1 0; -0.2051 -0.0996 0.0029];
imNew=imwarp(im, tform,'linear');
imNew = imresize(imNew,[1080 1920]);
%%
figure; subplot(1,2,1); imshow(im),title('Original Image'); subplot(1,2,2); imshow(imNew),title('Transformed image');

%%

        tm = [1 0 -0.00015*cosd(psi); 0 1 0.00015*sind(psi); 0 0 1]; %Transformation matrix
        tform = projective2d(tm); % Create a 2-D projective geometric transformation
        img_M = imwarp(img_M,tform,'nearest'); % Correct for linear projective warp
