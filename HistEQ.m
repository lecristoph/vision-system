    function img_out = HistEQ(img, Q)
    %%
    h = ones(5,5)/Q; %Lower denominator gives larger highlights
    x = size(img) ;
    
        for i = 1:x(3)       
         img = imfilter(img,h);
         img_out = histeq(img(:,:,i));
        end

        
    end